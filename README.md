# README #

### What is this repository for? ###

Playground for ELK. Includes logging client library, server and web site that logs to configured host/port.
You'll need to set up ELK stack on your local machine to run the website locally or point your configuration at existing 
ELK setup.

### How do I get set up? ###

Easiest way to get a logger is call the static method Logger.CreateUdpLogger(string hostname, int port)

### How do I log additional information specific to my application ###

You can extend LogEntry and add additional properties, all of which will be serialized and sent to ElasticSearch via Logstash. 
For example, if you're logging from a web app, you may want to create a WebLogEntry that contains info captured from the HTTP request.

Remember that every property serialzied is indexed by ElasticSearch and there fore queryable ;)

### Credit ###

Credit to Kristijan for initial implementation of Tools.Logging project. I had a few of these classes he wrote lying around and half of them missing. 
This is my attempt to piece it together again.