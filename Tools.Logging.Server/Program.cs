﻿using System;
using System.Configuration;

namespace Tools.Logging.Server
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var port = GetSetting("port", 9200);

            var udpServer = StartUdpServer(port);
            Console.WriteLine($"UDP server started on port {port}");

            Console.ReadKey();

            udpServer.Stop();

            Console.WriteLine($"{System.Environment.NewLine}Press any key to quit");
            Console.ReadKey();
        }

        private static UdpServer StartUdpServer(int port)
        {
            var udpServer = new UdpServer(port);
            udpServer.Start();
            return udpServer;
        }

        private static T GetSetting<T>(string key, T @default)
        {
            var value = ConfigurationManager.AppSettings[key];
            if (string.IsNullOrEmpty(value))
                return @default;

            try
            {
                return (T)Convert.ChangeType(value, typeof(T));
            }
            catch (InvalidCastException)
            {
                return @default;
            }
        }
    }
}
