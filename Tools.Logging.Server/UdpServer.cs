using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Tools.Logging.Server
{
    public class UdpServer
    {
        private readonly int _port;
        private Task _serverTask;
        private CancellationTokenSource _tokenSource;
        private UdpClient _udpClient;

        public UdpServer(int port)
        {
            _port = port;
            _tokenSource = new CancellationTokenSource();
        }

        public void Start()
        {
            if (_serverTask == null)
                _serverTask = Task.Factory.StartNew(StartUdpServer);
        }

        private void StartUdpServer()
        {
            _udpClient = new UdpClient(_port);
            
            while (true)
            {
                try
                {
                    var remoteIpEndpoint = new IPEndPoint(IPAddress.Any, 0);
                    var receiveBytes = _udpClient.Receive(ref remoteIpEndpoint);
                    var returnData = Encoding.ASCII.GetString(receiveBytes);

                    Console.WriteLine(returnData + System.Environment.NewLine);
                }
                catch (SocketException ex)
                {
                    if (ex.SocketErrorCode == SocketError.Interrupted) // connection was closed
                        break;

                    Console.WriteLine(ex.ToString());
                    throw;
                }
                catch (ObjectDisposedException)
                {
                    break; // connection was closed so don't call Receive again
                }
            }
        }

        public void Stop()
        {
            _udpClient.Close();
        }
    }
}