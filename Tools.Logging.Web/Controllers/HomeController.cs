﻿using System;
using System.Web.Mvc;
using Tools.Logging.Interfaces;
using Tools.Logging.Web.Infrastructure;
using Tools.Logging.Web.Models;

namespace Tools.Logging.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger _logger;

        public HomeController()
        {
            _logger = Logger.CreateUdpLogger(Settings.LoggingHost, Settings.LoggingPort);
        }

        public ActionResult Index()
        {
            var viewModel = TempData["viewModel"] as HomeViewModel ?? new HomeViewModel();
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Index(HomeViewModel viewModel)
        {
            var logEntryFunc = new Func<LogEntry>(() => viewModel.ThrowException
                ? ExceptionHelper.GetLogEntryFromException(viewModel.LogType, viewModel.Message)
                : new LogEntry(viewModel.LogType, viewModel.Message));

            _logger.Log(logEntryFunc);

            viewModel.MessageWasLogged = true;
            TempData["viewModel"] = viewModel;

            return RedirectToAction("Index");
        }
    }
}