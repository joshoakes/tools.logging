using System;
using System.Collections.Generic;

namespace Tools.Logging.Web.Infrastructure
{
    public class ExceptionHelper
    {
        private static readonly List<Action> ExceptionalActions;
        private static readonly Random IndexGenerator;

        static ExceptionHelper()
        {
            IndexGenerator = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);

            ExceptionalActions = new List<Action>()
            {
                () => { var outOfRange = "foo"[3]; },
                () => { var thingy = (object) null; thingy.ToString(); },
                () => { throw new ApplicationException("threw application exception"); },
                () => { throw new ArgumentException("threw argument exception", "arg"); },
                () => { throw new FormatException("threw format exception", new ArithmeticException("an inner expception")); },
            };
        }

        public static LogEntry GetLogEntryFromException(LogEntryType logType, string message)
        {
            try
            {
                var nextIndex = IndexGenerator.Next(0, ExceptionalActions.Count);
                ExceptionalActions[nextIndex]();
            }
            catch (Exception ex)
            {
                return new LogEntry(logType, message, ex);
            }

            return new LogEntry(logType, message);
        }
    }
}