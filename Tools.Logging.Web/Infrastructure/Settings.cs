﻿using System;
using System.Configuration;

namespace Tools.Logging.Web.Infrastructure
{
    public class Settings
    {
        public static string LoggingHost => GetSetting(nameof(LoggingHost), "localhost");
        public static int LoggingPort => GetSetting(nameof(LoggingPort), 5900);
        public static string KibanaUrl => GetSetting(nameof(KibanaUrl), "#");

        private static T GetSetting<T>(string key, T @default)
        {
            var value = ConfigurationManager.AppSettings[key];
            if (string.IsNullOrEmpty(value))
                return @default;

            try
            {
                return (T)Convert.ChangeType(value, typeof(T));
            }
            catch (InvalidCastException)
            {
                return @default;
            }
        }
    }
}