﻿using System;
using System.Linq;
using System.Web.Mvc;
using Tools.Logging.Web.Infrastructure;

namespace Tools.Logging.Web.Models
{
    public class HomeViewModel
    {
        public string KibanaUrl => Settings.KibanaUrl;


        public bool MessageWasLogged { get; set; }
        public LogEntryType LogType { get; set; }
        public string Message { get; set; }
        public bool ThrowException { get; set; }

        public SelectList LogTypes
        {
            get
            {
                var values = from LogEntryType e in Enum.GetValues(typeof(LogEntryType))
                             select new { Id = e, Name = e.ToString() };

                return new SelectList(values, "Id", "Name", LogEntryType.Info);
            }
        }
    }
}