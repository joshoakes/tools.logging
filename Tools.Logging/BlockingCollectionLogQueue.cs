﻿using System.Collections.Concurrent;
using Tools.Logging.Interfaces;

namespace Tools.Logging
{
	public class BlockingCollectionLogQueue : ILogQueue
	{
	    private readonly BlockingCollection<LogQueueEntry> _queue;

	    public BlockingCollectionLogQueue()
	    {
	        _queue = new BlockingCollection<LogQueueEntry>();
	    }

	    public int Count => _queue.Count;

	    public bool IsCompleted => _queue.IsCompleted;

	    public void Add(LogQueueEntry entry)
        {
            _queue.Add(entry);
        }

		public LogQueueEntry Take()
        {
            return _queue.Take();
        }

		public void Complete()
        {
            _queue.CompleteAdding();
        }
	}
}
