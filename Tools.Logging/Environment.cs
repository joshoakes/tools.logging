using System.Security.Principal;

namespace Tools.Logging
{
    public class Environment
    {
        public string Assembly { get; }
        
        public string AssemblyVersion { get; }
        
        public string Machine { get; }
        
        public string User { get; }

        private static readonly object SyncRoot = new object();
        private static Environment _instance;

        public static Environment Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncRoot)
                    {
                        if (_instance == null)
                            _instance = new Environment();
                    }
                }

                return _instance;
            }
        }

        private Environment()
        {
            User = WindowsIdentity.GetCurrent()?.Name;
            Machine = System.Environment.MachineName;

            // TODO this isn't right... think about what you're trying to capture here.
            var assembly = System.Reflection.Assembly.GetEntryAssembly()?.GetName() ?? System.Reflection.Assembly.GetCallingAssembly().GetName();
            Assembly = assembly.Name;
            AssemblyVersion = assembly.Version.ToString();
        }
    }
}