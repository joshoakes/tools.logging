﻿namespace Tools.Logging.Interfaces
{
	public interface ILogQueue
	{
		bool IsCompleted { get; }
		int Count { get; }
		void Add(LogQueueEntry entry);
		LogQueueEntry Take();
		void Complete();
	}
}