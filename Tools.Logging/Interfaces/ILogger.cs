﻿using System;

namespace Tools.Logging.Interfaces
{
	public interface ILogger
	{
		void Log(LogEntry entry);
		void Log(Func<LogEntry> entryFunc);
	}
}