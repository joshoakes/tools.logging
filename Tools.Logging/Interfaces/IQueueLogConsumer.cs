﻿using System.Threading.Tasks;

namespace Tools.Logging.Interfaces
{
	public interface IQueueLogConsumer
	{
		ILogQueue Queue { get; }
		void Start();
	}
}