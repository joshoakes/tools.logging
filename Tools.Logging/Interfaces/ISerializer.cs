﻿namespace Tools.Logging.Interfaces
{
	public interface ISerializer
	{
		string Serialize<T>(T entry);
	}
}