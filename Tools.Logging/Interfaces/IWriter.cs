﻿namespace Tools.Logging.Interfaces
{
	public interface IWriter
	{
		/// <summary>
		/// Writes given message to TCP socket.
		/// </summary>
		/// <param name="message">Message to write.</param>
		void Write(string message);
	}
}