﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Tools.Logging.Interfaces;

namespace Tools.Logging
{
    public class JsonSerializer : ISerializer
    {
        public string Serialize<T>(T entry)
        {
	        var settings = new JsonSerializerSettings
	        {
		        ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
		        Error = delegate(object sender, ErrorEventArgs args)
		        {
			        args.ErrorContext.Handled = true;
		        }
	        };

	        return JsonConvert.SerializeObject(entry, settings);
        }
    }
}