﻿using System;

namespace Tools.Logging
{
    /// <summary>
    /// Extend this class to add application specific properties
    /// </summary>
	public class LogEntry
    {
        public Environment Environment => Environment.Instance;

	    public DateTime Timestamp { get; }

	    private readonly LogEntryType _logEntryType;
	    public string LogEntryType => _logEntryType.ToString();

	    public string Message { get; }

		public Exception Exception { get; set; }

	    public LogEntry(LogEntryType entryType, string message)
		{
			Timestamp = DateTime.UtcNow;
			_logEntryType = entryType;
			Message = message;
		}

	    public LogEntry(LogEntryType entryType, string message, Exception exception) : this(entryType, message)
		{
			Exception = exception;
		}
	}
}