using System;

namespace Tools.Logging
{
	internal class LogEntryDetailsException : Exception
	{
		public LogEntryDetailsException(string message, Exception exception) : base(message, exception)
		{
			
		}
	}
}