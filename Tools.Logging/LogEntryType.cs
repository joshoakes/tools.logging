﻿namespace Tools.Logging
{
	public enum LogEntryType
	{
		Info,
		Debug,
		Warning,
		Error,
		Fatal
	}
}