using System;

namespace Tools.Logging
{
	public class LogQueueEntry
	{
		public LogQueueEntry(Func<LogEntry> logEntryFunc)
		{
			LogEntryFunc = logEntryFunc;
		}

		public Func<LogEntry> LogEntryFunc { get; }
	}
}