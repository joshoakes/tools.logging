﻿using System;

namespace Tools.Logging
{
	public static class ObjectExtensions
	{
		public static void ThrowIfNull(this object @object, string name)
		{
			if (@object == null)
				throw new NullReferenceException(name);
		}
	}
}