﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Tools.Logging.Interfaces;

namespace Tools.Logging
{
	public class QueueLogConsumer : IQueueLogConsumer
	{
		private readonly ISerializer _serializer;
	    private readonly IWriter _writer;
	    private Task _task;

	    public ILogQueue Queue { get; }

	    public QueueLogConsumer(ILogQueue queue, ISerializer serializer, IWriter writer)
        {
            queue.ThrowIfNull(nameof(queue));
            serializer.ThrowIfNull(nameof(serializer));
            writer.ThrowIfNull(nameof(writer));

            Queue = queue;
            _serializer = serializer;
            _writer = writer;
        }

        public void Start()
        {
            if (_task == null)
                _task = Task.Factory.StartNew(ProcessQueue);
        }

        private void ProcessQueue()
        {
            while (!Queue.IsCompleted)
            {
                var entry = Queue.Take();
                ProcessEntry(entry);
            }
        }

        private void ProcessEntry(LogQueueEntry queueEntry)
        {
            try
            {
                var entry = queueEntry.LogEntryFunc();
                var message = _serializer.Serialize(entry);
                Trace.WriteLine(message);
                _writer.Write(message);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);

				var warningEntry = new LogQueueEntry(
					() => new LogEntry(
						LogEntryType.Warning,
						"QueueLogConsumer unable to process log entry.",
						new LogEntryDetailsException(null, ex)));

                Queue.Add(warningEntry);
            }
        }
    }
}
