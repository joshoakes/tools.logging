﻿using System;
using Tools.Logging.Interfaces;
using Tools.Logging.Writers;

namespace Tools.Logging
{
	public class Logger : ILogger
	{
	    private readonly IQueueLogConsumer _consumer;

	    public Logger(IQueueLogConsumer consumer)
        {
            consumer.ThrowIfNull(nameof(consumer));
            _consumer = consumer;
            _consumer.Start();
        }

        public void Log(LogEntry entry)
        {
            if (entry == null)
                return;

            Log(() => entry);
        }

        public void Log(Func<LogEntry> entryFunc)
        {
            if (entryFunc == null)
                return;

            _consumer.Queue.Add(new LogQueueEntry(entryFunc));
        }

	    public static ILogger CreateUdpLogger(string hostname, int port)
	    {
	        var queue = new BlockingCollectionLogQueue();
	        var serializer = new JsonSerializer();
	        var writer = new UdpWriter(hostname, port);
            var consumer = new QueueLogConsumer(queue, serializer, writer);
	        return new Logger(consumer);
	    }
    }
}
