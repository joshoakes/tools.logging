﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using Tools.Logging.Interfaces;

namespace Tools.Logging.Writers
{
	/// <summary>
    /// Writer that uses TCP sockets to talk to server.
    /// </summary>
    public class TcpWriter : IWriter
	{
        private readonly int _connectionTimeout;
        private readonly string _address;
        private readonly int _port;

        private TcpClient _tcpClient;
        private StreamWriter _streamWriter;
        private Timer _connectionTimer;
        private bool _shouldReconnect = true;

        private readonly object _lock = new object();

        /// <summary>
        /// Initializes a new instance of the <see cref="TcpWriter" /> class.
        /// </summary>
        /// <param name="address">IP Address of the server, ie "10.211.123.42".</param>
        /// <param name="port">Port number that the server will listen to.</param>
        /// <param name="connectionTimeout">Connection timeout in seconds.</param>
        public TcpWriter(string address, int port, int connectionTimeout = 120)
        {
            address.ThrowIfNull("address");
            if (port < 1)
                throw new ArgumentOutOfRangeException(nameof(port));

            _address = address;
            _port = port;
            _connectionTimeout = connectionTimeout;
        }

        /// <summary>
        /// Writes given message to TCP socket.
        /// </summary>
        /// <param name="message">Message to write.</param>
        public void Write(string message)
        {
            EnsureConnectionTimer();

            lock (_lock)
            {
                EnsureConnection();
	            _streamWriter?.WriteLine(message);
            }
        }

        private void EnsureConnection()
        {
            if (!_shouldReconnect)
                return;

            _shouldReconnect = false;
            _tcpClient = new TcpClient();
            _tcpClient.Connect(_address, _port);
            _streamWriter = new StreamWriter(_tcpClient.GetStream()) { AutoFlush = true };
        }

        private void EnsureConnectionTimer()
        {
            if (_connectionTimer != null)
                return;

            _connectionTimer = new Timer(ConnectionTimerCallBack);
            _connectionTimer.Change(_connectionTimeout * 1000, _connectionTimeout * 1000);
        }

        private void ConnectionTimerCallBack(object state)
        {
            lock (_lock)
            {
                if (_streamWriter != null)
                {
                    _streamWriter.Close();
                    _streamWriter = null;
                }

                _shouldReconnect = true;
            }
        }
    }
}
