﻿using System.Diagnostics;
using Tools.Logging.Interfaces;

namespace Tools.Logging.Writers
{
    public class TraceWriter : IWriter
    {
        public void Write(string message)
        {
            if (!string.IsNullOrWhiteSpace(message))
                Trace.WriteLine(message);
        }
    }
}
