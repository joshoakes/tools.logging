﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Tools.Logging.Interfaces;

namespace Tools.Logging.Writers
{
    public class UdpWriter : IWriter
    {
        private readonly string _hostname;
        private readonly int _port;
        private readonly int _numberOfRetries;

        private UdpClient _udpClient;

        public UdpWriter(string hostname, int port, int numberOfRetries = 5)
        {
            if (port < IPEndPoint.MinPort || port > IPEndPoint.MaxPort)
                throw new ArgumentOutOfRangeException(nameof(port));

            if (string.IsNullOrEmpty(hostname))
                throw new ArgumentException(nameof(hostname));

            _hostname = hostname;
            _port = port;
            _numberOfRetries = numberOfRetries;

            InitConnection();
        }

        public void Write(string message)
        {
            var bytes = Encoding.ASCII.GetBytes(message);

            var success = false;
            var numberOfBytesSent = 0;
            var numberOfRetries = _numberOfRetries;

            while (!success && numberOfRetries > 0)
            {
                numberOfBytesSent += _udpClient.Send(bytes, bytes.Length);
                success = numberOfBytesSent >= bytes.Length;
                numberOfRetries--;
            }

            // This acts as a form of flow control to help prevent network
            // switches or routers from overflowing their internal buffers.
            Thread.Sleep(2);
        }

        private void InitConnection()
        {
            _udpClient = new UdpClient();
            _udpClient.Connect(_hostname, _port);
        }
    }
}
